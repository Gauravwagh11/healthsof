from __future__ import unicode_literals

from django.db import models
from model_utils import Choices
import datetime

# Local imports
from healthsoft.case.models import Case
from healthsoft.drugs.models import Drug
# Create your models here.

class Treatment(models.Model):
    STATUS_CHOICE = Choices(('Open', 'Open'), ('Close', 'Close'), ('Pending', 'Pending'), ('In Progress', 'In Progress'))

    case = models.ForeignKey(Case)
    treatment_details = models.TextField(null=True, blank=True)
    status = models.CharField(choices=STATUS_CHOICE, blank=True, max_length=255)
    followup_status = models.BooleanField(default=False)
    created = models.DateTimeField(default=datetime.datetime.now)
    modified = models.DateTimeField()


class TreatmentDrug(models.Model):
    treatment = models.ForeignKey(Treatment)
    drug = models.ForeignKey(Drug)
    created = models.DateTimeField(default=datetime.datetime.now)
    modified = models.DateTimeField()

class FollowUp(models.Model):
    FOLLOWUP_CHOICE = Choices(('Open', 'Open'), ('Close', 'Close'), ('In Progress', 'In Progress'))

    treatment = models.ForeignKey(Treatment, null=True, blank=True)
    next_appointment_date = models.DateTimeField(null=True, blank=True)
    followup_status = models.CharField(choices=FOLLOWUP_CHOICE, blank=True, max_length=255)
    created = models.DateTimeField(default=datetime.datetime.now)
    modified = models.DateTimeField()

