from __future__ import unicode_literals

from django.db import models
import datetime
from django_extensions.db.models import TitleSlugDescriptionModel


from .utils import increment_drug_id
# Create your models here.

class Drug(TitleSlugDescriptionModel):
    drug_id =  models.CharField(max_length=500, default=increment_drug_id, null=True, blank=True)
    manufacturer = models.CharField(max_length=250, null=True, blank=True)
    created = models.DateTimeField(default=datetime.datetime.now)
    modified = models.DateTimeField()
