from django.apps import apps


# Creating unique patient ID *********



def increment_doctor_id():
    Doctor = apps.get_model('doctor', 'Doctor')
    last_doctor = Doctor.objects.all().order_by('id').last()
    if not last_doctor:
        return 'DRI0001'
    doctor_id = last_doctor.patient_id
    doctor_int = int(doctor_id.split('DRI')[-1])
    width = 4
    new_doctor_int = doctor_int + 1
    formatted = (width - len(str(new_doctor_int))) * "0" + str(new_doctor_int)
    new_doctor_id = 'DRI' + str(formatted)
    return new_doctor_id
