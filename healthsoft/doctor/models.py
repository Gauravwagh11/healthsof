from __future__ import unicode_literals

from django.db import models

# Create your models here.

import datetime
from model_utils import Choices
from django.db import models
from django.contrib.contenttypes.models import ContentType
# from django.contrib.contenttypes import

from .utils import increment_doctor_id
from healthsoft.users.models import User

# Create your models here.

class Doctor(models.Model):

    BLOOD_GROUP = Choices(('+A', '+A'), ('-A', '-A'), ('+B', '+B'), ('-B', '-B'), ('+AB', '+AB'), ('-AB', '-AB'),
                          ('+O', '+O'), ('-O', '-O'))

    user = models.ForeignKey(User)
    doctor_id = models.CharField(max_length=500, default=increment_doctor_id, null=True, blank=True)
    blood_group = models.CharField(choices=BLOOD_GROUP, blank=True, max_length=255)
    created = models.DateTimeField(default=datetime.datetime.now)
    modified = models.DateTimeField()

    def __unicode__(self):
        return self.doctor_id



class EducationManager(models.Manager):

    def get_queryset(self):
         qs = super(EducationManager,self).get_queryset()
         return qs.order_by('-created')        # order the queryset

    # def add(self, induser, **kwargs):
    #     education = Education(content_object = induser)
    #     education.save()
    #     return education

    def education_list(self, doctor):
        educations = doctor.education.all()
        return educations


    # def get_or_create(self, induser, **kwargs):
    #     if not induser.education.all():
    #         return self.add(induser, **kwargs), True
    #     return induser.education.all(), False

    def delete(self, doctor):
        doctor.education.all().delete()
        return True

    def by_id(self, id):
        """
        Get  by id
        """
        return self.get(id=id)

class Education(models.Model):
    degree = models.CharField(max_length=1000, blank=True, null=True)
    school_name = models.CharField(max_length=1000, blank=True, null=True)
    field_of_studies = models.CharField(max_length=1000, blank=True, null=True)
    start_year = models.DateField(blank=True, null=True)
    end_year = models.DateField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    other_details = models.TextField(null=True, blank=True)

    doctor = models.ForeignKey(Doctor, null=True, blank=True)
    objects = EducationManager()

    def __unicode__(self):
        return self.degree




class ExceperienceManager(models.Manager):

    def get_queryset(self):
         qs = super(ExceperienceManager,self).get_queryset()
         return qs.order_by('-created')        # order the queryset

    # def add(self, doctor, **kwargs):
    #     experience = Experience(doctor = doctor)
    #     experience.save()
    #     return experience

    def exceperience_list(self, doctor):
        experience = doctor.exp.all()
        return experience

    def get(self, doctor):
        exceperience = self.get(doctor = doctor)
        return exceperience

    # def get_or_create(self, induser, **kwargs):
        #
        # if not induser.exp.all():
        #     return self.add(induser, **kwargs), True
        # return induser.exp.all(), False

    def delete(self, induser):
        induser.exp.all().delete()
        return True


class Experience(models.Model):
    details = models.TextField(blank=True, null=True)
    field_of_work = models.CharField(max_length=1000, blank=True, null=True)
    start_year = models.DateField( blank=True, null=True)
    end_year = models.DateField(blank=True, null=True)

    created = models.DateTimeField(auto_now_add=True)

    doctor = models.ForeignKey(ContentType, null=True, blank=True)
    objects = ExceperienceManager()

    def __unicode__(self):
        return self.company_name
