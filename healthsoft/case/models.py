from __future__ import unicode_literals

from django.db import models
from model_utils import Choices
import datetime
from django_extensions.db.models import TitleSlugDescriptionModel


from .utils import increment_case_id
from healthsoft.doctor.models import Doctor
from healthsoft.patient.models import Patient

# Create your models here.

class Case(TitleSlugDescriptionModel):
    doctor = models.ForeignKey(Doctor)
    patient = models.ForeignKey(Patient)
    case_id = models.CharField(max_length=500, default=increment_case_id, null=True, blank=True)
    created = models.DateTimeField(default=datetime.datetime.now)
    modified = models.DateTimeField()

    def __unicode__(self):
        return self.patient_id
