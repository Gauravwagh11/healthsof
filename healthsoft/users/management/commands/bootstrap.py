from django.core.management.base import BaseCommand

from healthsoft.users.models import User

class Command(BaseCommand):
    help = 'Update closing time for each show  '

    def handle(self, *args, **options):
        try:
            User.objects.create_superuser('admin', 'admin@healthsoft.com', 'healthsoftadmin')
            self.stdout.write(self.style.SUCCESS('Successfully created superuser.'))
        except:
            self.stdout.write(self.style.SUCCESS('Superuser is already created.'))

        self.stdout.write(self.style.SUCCESS('Successfully created Data.'))

