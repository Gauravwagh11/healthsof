# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.contrib.auth.models import AbstractUser
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from model_utils import Choices


@python_2_unicode_compatible
class User(AbstractUser):

    # First Name and Last Name do not cover name patterns
    # around the globe.

    GENDER = Choices(('male', 'Male'), ('female', 'Female'))
    MARRIED = Choices(('yes', 'Yes'), ('no', 'No'))

    name = models.CharField(_('Name of User'), blank=True, max_length=255)
    phone = models.CharField(_("Mobile Number"), max_length=50, blank=True, null=True)
    birthday = models.DateField(_("Date of Birth"), blank=True, null=True)
    photo = models.ImageField(_("Profile Photo"), blank=True, null=True, max_length=255)
    gender = models.CharField(choices=GENDER, blank=True, null=True, max_length=30)


    def __str__(self):
        return self.username

    def get_absolute_url(self):
        return reverse('users:detail', kwargs={'username': self.username})
