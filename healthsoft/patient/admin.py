from django.contrib import admin

# Register your models here.

from healthsoft.patient.models import Patient

admin.site.register(Patient)
