from django.apps import apps


# Creating unique patient ID *********



def increment_patient_id():
    Patient = apps.get_model('patient', 'Patient')
    last_patient = Patient.objects.all().order_by('id').last()
    if not last_patient:
        return 'PDT0001'
    patient_id = last_patient.patient_id
    patient_int = int(patient_id.split('PDT')[-1])
    width = 4
    new_patient_int = patient_int + 1
    formatted = (width - len(str(new_patient_int))) * "0" + str(new_patient_int)
    new_patient_id = 'PDT' + str(formatted)
    return new_patient_id
