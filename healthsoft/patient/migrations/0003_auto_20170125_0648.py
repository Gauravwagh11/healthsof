# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-01-25 06:48
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('patient', '0002_auto_20161202_1044'),
    ]

    operations = [
        migrations.AddField(
            model_name='patient',
            name='created',
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
        migrations.AddField(
            model_name='patient',
            name='modified',
            field=models.DateTimeField(default=datetime.datetime.now),
            preserve_default=False,
        ),
    ]
