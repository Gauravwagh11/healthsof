# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-02 10:44
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import healthsoft.patient.utils


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('patient', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='patient',
            name='user',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='patient',
            name='patient_id',
            field=models.CharField(blank=True, default=healthsoft.patient.utils.increment_patient_id, max_length=500, null=True),
        ),
    ]
