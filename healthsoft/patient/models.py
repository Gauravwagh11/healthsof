from __future__ import unicode_literals

from django.db import models
from model_utils import Choices

import datetime
from .utils import increment_patient_id
from healthsoft.users.models import User

# Create your models here.

class Patient(models.Model):

    BLOOD_GROUP = Choices(('+A', '+A'), ('-A', '-A'), ('+B', '+B'), ('-B', '-B'), ('+AB', '+AB'), ('-AB', '-AB'),
                          ('+O', '+O'), ('-O', '-O'))

    user = models.ForeignKey(User)
    patient_id = models.CharField(max_length=500, default=increment_patient_id, null=True, blank=True)
    blood_group = models.CharField(choices=BLOOD_GROUP, blank=True, max_length=255)
    created = models.DateTimeField(default=datetime.datetime.now)
    modified = models.DateTimeField()

    def __unicode__(self):
        return self.patient_id
